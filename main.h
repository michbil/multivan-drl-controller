#include <18f258.h>
#use delay(clock=16000000,RESTART_WDT)
#FUSES	H4,NOWDT,PROTECT



#define MSG_RPM 0x35b
#define MSG_LIGHT 0x531

#define MSG_ID1 MSG_RPM
#define MSG_ID2 MSG_LIGHT
#define MSG_ID3 MSG_RPM

#define CAN_USE_EXTENDED_ID 0
#include "can-18xxx8.h"
#include "can-18xxx8.c"



#define LED1	PIN_C4
#define LED2	PIN_C5

#define DRL_ON  PIN_C3

#define AUX_IN1 PIN_A0
#define AUX_IN2 PIN_A1


#define POWER_HOLD PIN_C6
#define CAN_EN PIN_B4
#define CAN_ERR PIN_B1
#define CAN_STB PIN_B0