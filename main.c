// vag multivan drl controller 

#include "main.h"

char t_bit,r_bit1,r_bit2,r_bit3;
unsigned int ms_cnt;
unsigned long packet1_timer,packet2_timer,sleep_timer;

// ��������� ������  ---  ��������� ����������

int engine_started = 0;
int light_on = 0;

#define TIMER_MAX 40000

unsigned long pwm_duty = 20000;
unsigned long pwm_work_duty = 0;

int pwm_phase = 0;
int sleep_mode = 0;



#INT_TIMER1
void pwm_isr() {




    if (pwm_phase == 0) {
        pwm_work_duty = pwm_duty;



        set_timer1(65535 - (pwm_work_duty));

        
        pwm_phase=1;


       if (pwm_work_duty < 1000) {
            output_high(PIN_C2);

        } else output_low(PIN_C2);


    } else {
        set_timer1(65535 - (TIMER_MAX - pwm_work_duty));
        pwm_phase=0;


        output_high(PIN_C2);
        
    }

    

}


#INT_RTCC
void ms_isr() {

	ms_cnt++;
	if (ms_cnt > 100) {
		ms_cnt=0;
		t_bit=1;
	}
	
	if ((ms_cnt % 10)==0) {
		output_low(LED1);
		//output_low(LED2);
	}

        packet1_timer++;
        packet2_timer++;

        if (packet1_timer > 5000) {
            engine_started=0;
        }

        if (packet2_timer > 5000) {
            //light_on=0;
        }
        sleep_timer++;
        if (sleep_timer > 20000) {
            sleep_mode=1;
            sleep_timer=0;

            //reset_cpu();
        }

}

void power_init() {
    output_low(POWER_HOLD);
    output_high(CAN_STB);
    output_high(CAN_EN);
}

void power_shutdown() {

    output_high(POWER_HOLD);
    output_high(LED1);
    output_high(LED2);
    delay_ms(100);

    output_low(CAN_STB);

    while(1) {
        output_high(CAN_STB);
        output_high(CAN_EN);
        output_high(LED1);
        delay_ms(200);
        output_low(LED1);
        delay_ms(200);

        output_low(CAN_STB);
        delay_ms(200);
    }
    
    
/*
    delay_ms(1000);

    while (1) {

        power_init();
        delay_ms(20);
        output_low(CAN_STB);
        delay_ms(500);


   }

*/

}

void init_hw() {

    power_init();
    output_high(LED1);
    output_high(LED2);
    delay_ms(100);
    output_low(LED1);
    //output_low(LED2);


    setup_timer_0(RTCC_INTERNAL|RTCC_DIV_4 | RTCC_8_BIT);
    setup_timer_1(T1_INTERNAL | T1_DIV_BY_1);
    
    setup_adc(ADC_OFF);
    can_init(BAUD_100kbps);


    enable_interrupts(INT_RTCC);
    enable_interrupts(INT_TIMER1);
    enable_interrupts(GLOBAL);

}

void set_light(long bright) {

    if (bright == 0) {
        output_low(DRL_ON);
        pwm_duty=500;
    } else {
        output_high(DRL_ON);
        pwm_duty = bright;
    }

}

void main()
{
    struct rx_stat rxstat;
    int32 rx_id;
    int rx_len;
    unsigned char data[8];

    init_hw();
    set_light(0);
    sleep_timer=0;

    while(1) {
    restart_wdt();

    if (sleep_mode) {
        if (!light_on) {
            power_shutdown();
        } else {
            sleep_mode=0;
        }
        

    }

    //if (light_on || engine_started) output_high(LED1); else output_low(LED1);
    //if (engine_started) output_high(LED2); else output_low(LED2);

    if ((light_on) | (engine_started)) {
        if (light_on & 2)
             set_light(10000); // dim the light
        else
            set_light(20000); // full light

    } else set_light(0);



        
        if (can_kbhit()) {
            if(can_getd(rx_id, &data[0], rx_len, rxstat)) {

                switch (rx_id) {

                    case MSG_RPM:
                       
                        output_high(LED1);
                        packet1_timer=0;

                        if (data[2] > 0) {
                            engine_started=1;
                        } else {
                            engine_started=0;
                        }
                        sleep_timer=0;

                        restart_wdt();

                       
                    break;

                    case MSG_LIGHT:

                        output_high(LED1);
                        packet2_timer=0;

                        if (data[0] & 1) {
                            light_on = data[0];

                        } else {
                            light_on = 0;
                        }
                        sleep_timer=0;

                        restart_wdt();
                    default:
                    break;
                } // switch

            };  //if
        }; // if


    } // while
} // main